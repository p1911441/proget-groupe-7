window.addEventListener("load", loaded, false);


let list = Possibilities;
let listPays = LstPays;
let listGraph = [];


function loaded() {
    
    let select = document.getElementById("select");
    select.addEventListener("change", setChart);


    for (const name of list) {
        let op = document.createElement("option");
        op.innerText = name;
        select.appendChild(op);
    }

    


    function setChart(){

        let select = document.getElementById("select");
        let div = document.getElementById("graph").getContext('2d');

        let selectPays = document.getElementById("pays")
        if(selectPays != null) {
            selectPays.parentNode.removeChild(selectPays);
        }
        let selectDisc = document.getElementById("disciplines")
        if(selectDisc != null) {
            selectDisc.remove()
        }

        for(const graph of listGraph) {
            graph.destroy();
        }
        listGraph = [];

        let option = select.options[select.selectedIndex].value;

        if(list[0] == option) {
            
            var myChart = new Chart(div, {
                type: 'bar',
                data: {
                    labels: listPays,
                    datasets: [{
                        label: 'nombre de médailles',
                        data: AllMedals,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        x: {
                            ticks: {
                                autoSkip: false
                            }
                        },
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            listGraph.push(myChart);
        }
        else if(list[1] == option) {
            if(document.getElementById("pays") == null) {
                let pays = document.createElement("select");
                pays.setAttribute("id", "pays");
                                
                for (const name of listPays) {
                    let op = document.createElement("option");
                    op.innerText = name;
                    pays.appendChild(op);
                }

                pays.addEventListener("change", updateDisc);
                pays.addEventListener("change", GraphPaysDiscipline);
                document.getElementById("div_select").appendChild(pays);
            }
            updateDisc();

            GraphPaysDiscipline();
        }



    }

    function updateDisc(){
        if(document.getElementById("disciplines") != null)
            document.getElementById("disciplines").remove();
        let disc = document.createElement("select");
        disc.setAttribute("id", "disciplines");

        for (const discipline of GetDiscListForCountry()) {
            let op = document.createElement("option");
            op.innerText = discipline;
            disc.appendChild(op);
        }
        
        document.getElementById("div_select").appendChild(disc);
        disc.addEventListener("change", GraphPaysDiscipline);
    }

    function GetDiscListForCountry(){
        let selectPays = document.getElementById("pays");
        let optionPays = selectPays.options[selectPays.selectedIndex].innerText;
        let dislist = [];
        for(const [key, value] of Object.entries(AllData[optionPays]['disciplines'])){
            dislist.push(key);
        }
        return dislist;
    }

    function GraphPaysDiscipline() {

        let div = document.getElementById("graph").getContext('2d');

        let selectPays = document.getElementById("pays");
        let optionPays = selectPays.options[selectPays.selectedIndex].innerText;

        let selectDisc = document.getElementById("disciplines");
        let optionDisc = selectDisc.options[selectDisc.selectedIndex].innerText;

        for(const graph of listGraph) {
            graph.destroy();
        }

        listGraph = [];
        let Ratio = [];
        try{
            Ratio = [   parseInt(AllData[optionPays]['disciplines'][optionDisc]['nbrWomen']),
                        parseInt(AllData[optionPays]['disciplines'][optionDisc]['nbrMen'])  ]
        }
        catch{
            console.log("Error")
            console.log("optionPays : " + optionPays);
            console.log("optionDisc : " + optionDisc);
        }

        var myChart = new Chart(div, {
            type: 'doughnut',
            data: {
                labels: ['Femme', 'Homme'],
                datasets: [{
                    label: 'repartition homme femme dans le badminton en France',
                    data: Ratio,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)'
                    ],
                    hoverOffset: 4
                }]
            }
        });
        listGraph.push(myChart);
    }

}

