window.addEventListener("load", loaded, false);

// Tableau des pays colorés
// utile pour les décolorer par la suite
var colored = [];
// Tableau des 3 étapes de coloration
var Colors = ["#113e51","rgb(53,115,128)","rgb(118,190,214)"]

// Lancée une fois la page chargée
function loaded() {
    // Events On change sur les 2 select
    let selectD = document.getElementById("select_discipline");
    let selectG = document.getElementById("select_genre");
    selectD.addEventListener("change", onChange);
    selectG.addEventListener("change", onChange);
    onChange();
}

// Lors d'un changement sur un select :
function onChange() {
    let selectD = document.getElementById("select_discipline");
    let selectG = document.getElementById("select_genre");
    
    // Si Homme :
    if(selectG.options[selectG.selectedIndex].value == "Homme"){
        color(tab[selectD.options[selectD.selectedIndex].value]['BestMen'])
    }
    // Si Femme :
    else{
        color(tab[selectD.options[selectD.selectedIndex].value]['BestWomen'])
    }
}

function color(countryList) {
    // On remet à zero la coloration :
    for(const c of colored) {
        c.removeAttribute("style");
    }
    colored = [];
    let colorId = 0;
    // On colore la liste :
    for(country of countryList){
        if (country != null) {

            let elements = document.getElementsByClassName(country);
            for (const e of elements) {
                if (e.getAttribute("style") != null) {
                    e.removeAttribute("style");
                } else {
                    e.setAttribute("style", "fill:" + Colors[colorId]);
                    colored.push(e);
                }
            }
            colorId++;
        }
    }
}

