from django.shortcuts import render
from .model import Queries as qr
from .model import CountryList as cl
from .model import ListDiscipline as ld


def view_accueil(request):
    context = {
    }
    return render(request, 'acceuil.html', context)

def view_carte(request):
    # lecture de la carte SVG :
    CarteSvg=""
    with open("./Projet7/static/Svg/carteSVG.txt", "r") as f:
        CarteSvg = f.read()

    # Création de la liste de discipines
    # et de leurs mailleurs pays 
    LD = ld.ListDiscipline()
    LD.AddDisciplines(qr.query_GetDisciplinesBest())

    context = {
        "DisciplinesBestCountries" : LD.ToJson(),
        "ListeDiscipline" : list(LD.list.keys()),
        "CarteSvg": CarteSvg
    }
    return render(request, 'carte.html', context)

def view_graphe(request):

    # Création de la liste de pays avec leurs médailles
    # et leurs disciplines
    CL = cl.countryList()
    CL.AddMedals(qr.query_GetMedals())
    CL.AddDisciplines(qr.query_GetDisciplines())
    CL.SortByMedals()

    context = {
        'countries' : CL.GetCountryList(),
        'Medals' : CL.GetNbrMedalList(),
        'AllData' : CL.ToJson()
    }
    return render(request, 'graphe.html', context)
