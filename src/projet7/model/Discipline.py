class Discipline:
    """
    classe conservant le nombre d'homme et de femmes enregistrés pour elle
    """

    def __init__(self) -> None:
        self.nbr_men = 0
        self.nbr_women = 0

    def addAthlete(self,line : str) -> None:
        """
        méthode ajoutant un nombre d'homme ou de femme à la discipline
        avec une liste d'infos

        @param line : de la forme [country, genre, nombre d'athlete, nom discipline]
        """
        if line[1] == 'Female':
            #print(f"added {line[2]} females in {line[3]} for {line[0]}")
            self.nbr_women += int(line[2])
        else:
            #print(f"added {line[2]} males in {line[3]} for {line[0]}")
            self.nbr_men += int(line[2])

    def ToJson(self) -> dict:
        """
        méthode permettant de renvoyer le json correspondant à la liste
        """
        dic = {}
        dic['nbrMen'] = self.nbr_men
        dic['nbrWomen'] = self.nbr_women
        return dic

class Discipline_BestCountries:
    """
    classe conservant une discipline avec ses 3 meilleurs pays
    """

    #country gender medal discipline
    def __init__(self) -> None:
        self.listCountryMen = ["","",""]
        self.listMedlasMen = [0, 0, 0]
        self.listCountryWomen = ["","",""]
        self.listMedalsWomen = [0, 0, 0]

    def AddData(self,line) -> None:
        """
        méthode permattant de confronter une nouvelle donnée à celles 
        existantes afin de voir si elle a sa place dans le top 3
        """
        listMedal = None
        listCountry = None
        # On travaille sur la bonne liste :
        if line[1] == 'Female':
            listMedal = self.listMedalsWomen
            listCountry = self.listCountryWomen
        else:
            listMedal = self.listMedlasMen
            listCountry = self.listCountryMen
        
        listMedal.append(int(line[2]))
        listCountry.append(line[0])

        # -- tri bulle --
        changement = True
        while changement:
            changement = False
            for i in range(3):
                if listMedal[i] < listMedal[i+1]:
                    listMedal[i], listMedal[i+1] = listMedal[i+1], listMedal[i]
                    listCountry[i], listCountry[i+1] = listCountry[i+1], listCountry[i]
                    changement = True
        listMedal = listMedal[:3]
        listCountry = listCountry[:3]

        # On remet dans la bonne liste :
        if line[1] == 'Female':
            self.listMedalsWomen = listMedal
            self.listCountryWomen = listCountry
        else:
            self.listMedlasMen = listMedal
            self.listCountryMen = listCountry

    def ToJson(self) -> dict:
        """
        méthode permettant de renvoyer le json de la discipline
        """
        return {
            'BestMen' : self.listCountryMen,
            'BestWomen' : self.listCountryWomen
        }
