from django.db import connections

def query_GetMedals() -> list:
    """
    fonction retournant le résultat de la query des médailles :

    @return : [[country, nbr bronze, nbr Silver, nbr Gold]...]

    """
    return Query("select mt.Country, mt.Bronze_Medal, mt.Silver_Medal, mt.Gold_Medal from medals_total mt")

def query_GetDisciplines() -> list:
    """
    fonction retournant le résultat de la query des disciplines :

    @return : [[country, genre, nombre d'athlete, nom discipline]...]

    """
    return Query("select country, gender, count(id), discipline  from athletes a group by country, gender, discipline")

def query_GetDisciplinesBest() -> list:
    """
    fonction retournant le résultat de la query des meilleurs pays par discipline :

    @return : [[country, genre, nombre de médaille, nom discipline]...]

    """
    return Query("select a.country, a.gender, count(m.id) medals, a.discipline from medals m, athletes a where m.athlete_id = a.id group by a.country, a.gender, a.discipline")

def Query(strQuery : str)-> list:
    """
    fonction permettant de se connecter à la base de donnée
    et d'executer la requête passée en parametre

    @param strQuery : requête à exécuter
    """
    c = connections['default'].cursor()
    c.execute(strQuery)
    rows = c.fetchall()
    return rows