import json
from . import Discipline

class country:
    """
    Classe permettant de stocker les infos relatives à un pays
    -> nombre de médailles et disciplines(nbrHomme et nbrFemme)
    """

    def __init__(self) -> None:
        self.nbr_medals = 0
        self.Disiplines = {}

    def addMedals(self, line : str) -> None:
        """
        méthode permettant d'ajouter une médaille avec une liste de type :
        [country, nbr bronze, nbr Silver, nbr Gold]
        """
        self.nbr_medals += (int(line[1]) + int(line[2]) + int(line[3]))

    def addDiscipline(self, line : str):
        """
        méthode permettant d'ajouter une discipline avec une liste de type :
        [country, genre, nombre d'athlete, nom discipline]
        """
        discipline = line[3]
        if discipline not in self.Disiplines:
            self.Disiplines[discipline] = Discipline.Discipline()
        self.Disiplines[discipline].addAthlete(line)

    def ToJson(self) -> dict :
        """
        méthode renvoyant le json de la discipline
        """
        dic = {}
        dic['nbrMedal'] = self.nbr_medals
        dic['disciplines'] = {}
        for key, obj in self.Disiplines.items():
            dic['disciplines'][key] = obj.ToJson()
        return dic


class countryList:
    """
    classe permettant de stocker une liste de pays
    """

    def __init__(self) -> None:
        self.list = {}

    def AddMedals(self, Lines : list) -> None:
        """
        méthode permettant d'ajouter une liste de données de type :
        [[country, nbr bronze, nbr Silver, nbr Gold]...]
        """
        for line in Lines:
            CountryCode = line[0]
            if CountryCode not in self.list:
                self.list[CountryCode] = country()
            self.list[CountryCode].addMedals(line)

    def AddDisciplines(self, Lines : list):
        """
        méthode permettant d'ajouter une liste de diciplines de type :
        [[country, genre, nombre d'athlete, nom discipline]...]
        """
        for line in Lines:
            CountryCode = line[0]
            if CountryCode not in self.list:
                self.list[CountryCode] = country()
            self.list[CountryCode].addDiscipline(line)
    
    def SortByMedals(self) -> None:
        """
        méthode permettant de trier le dictionnaire par médaille
        grace à l'algorithme 'Quick sort'
        """
        keyList = list(self.list.keys())
        self.quickSort(0,len(self.list)-1, keyList)
        newDict = {}
        for i in range(len(keyList)):
            newDict[keyList[i]] = list(self.list.items())[i][1]
        self.list = newDict
    
    def quickSort(self, low : int, high : int, keyList : list):
        """
        méthode récursive symbolisant une étape du Quick sort
        Elle utilise la liste des clés puisque le tri se fait sur un dictionnaire
        """
        if len(self.list) <= 1:
            return self.list
        if low < high:
            i = (low-1)
            pivot = list(self.list.items())[high][1].nbr_medals
            for j in range(low, high):
                if list(self.list.items())[j][1].nbr_medals <= pivot:
                    i = i+1
                    keyList[i], keyList[j] = keyList[j], keyList[i]
                    list(self.list.items())[i][1].nbr_medals, list(self.list.items())[j][1].nbr_medals = list(self.list.items())[j][1].nbr_medals, list(self.list.items())[i][1].nbr_medals

            keyList[i+1], keyList[j+1] = keyList[j+1], keyList[i+1]
            list(self.list.items())[i+1][1].nbr_medals, list(self.list.items())[high][1].nbr_medals = list(self.list.items())[high][1].nbr_medals, list(self.list.items())[i+1][1].nbr_medals
            pi = i+1

            self.quickSort(low, pi-1, keyList)
            self.quickSort(pi+1, high, keyList)
  

    def GetCountryList(self) -> str:
        """
        méthode permettant de récupérer le json de la liste des pays
        """
        cList = []
        for countryName, country in self.list.items():
            if country.nbr_medals > 0:
                cList.append(countryName)
        return json.dumps(cList)

    def GetNbrMedalList(self) -> str:
        """
        méthode permettant de récupérer le json de la liste de médailles
        """
        Data = []
        for c in self.list.values():
            Data.append(c.nbr_medals)
        return json.dumps(Data)
    
    def ToJson(self) -> str:
        """
        méthode permettant de récupérer la liste complete des infos des pays en Json
        """
        Dict = {}
        for k, v in self.list.items():
            Dict[k] = v.ToJson()
        return json.dumps(Dict)