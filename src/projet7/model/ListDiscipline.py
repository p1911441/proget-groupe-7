from . import Discipline
import json

class ListDiscipline:
    """
    Classe contenant une liste de disciplines associées 
    à leurs 3 pays les plus forts
    """

    def __init__(self) -> None:
        self.list = {}

    def AddDisciplines(self, lines) -> None:
        """
        méthode permettant de remplir la liste à l'aide d'une entrée
        de type : [[country, genre, nombre de médaille, nom discipline]...]
        """
        for line in lines:
            discipline = line[3]
            if discipline not in self.list:
                self.list[discipline] = Discipline.Discipline_BestCountries()
            self.list[discipline].AddData(line)

    def ToJson(self) -> str:
        """
        méthode permettant de renvoyer le json correspondant à la liste
        """
        dic = {}
        for nameDis, ObjDis in self.list.items():
            dic[nameDis] = ObjDis.ToJson()
        return json.dumps(dic)
